# kubernetes-do

At some point, automate the configuration of my DigitalOcean managed Kubernetes cluster. It seems like [Terraform has
functionality](https://developer.hashicorp.com/terraform/tutorials/kubernetes/kubernetes-provider) to do this nicely.
Ansible can also do this, but it seems a little hacky.

## TODO

- Add metrics server:

```shell
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm upgrade --install metrics-server metrics-server/metrics-server
 ```

https://artifacthub.io/packages/helm/metrics-server/metrics-server

- Install Nginx ingress controller (load balancer):

```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true
 ```

https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm#step-2-installing-the-kubernetes-nginx-ingress-controller

- Add certs:

```shell
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.10.1 --set installCRDs=true
 ``` 

https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm#step-4-securing-the-ingress-using-cert-manager
